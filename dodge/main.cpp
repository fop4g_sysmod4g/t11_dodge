
#include "Game.h"

using namespace sf;
using namespace std;




/*
* A version of the shootemup, this one has asteroids and juicy movement
*/
int main()
{
	// Create the main window
	RenderWindow window(VideoMode(GC::SCREEN_RES.x, GC::SCREEN_RES.y), "Rock Dodger");

	Game game;
	game.Init(window);
	//PlaceRocks(window, texRock, objects);

	Clock clock;

	// Start the game loop 
	while (window.isOpen())
	{
		// Process events
		Event event;
		while (window.pollEvent(event))
		{
			// Close window: exit
			if (event.type == Event::Closed) 
				window.close();
			if (event.type == Event::TextEntered)
			{
				if (event.text.unicode == GC::ESCAPE_KEY)
					window.close(); 
			}
		} 

		// Clear screen
		window.clear();

		float elapsed = clock.getElapsedTime().asSeconds();
		clock.restart();
		
		game.Update(window, elapsed);
		game.Render(window, elapsed);

		// Update the window
		window.display();
	}

	return EXIT_SUCCESS;
}
